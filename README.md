# scRNA-seq pipeline
<b>Author: Mariana Ribeiro</b>

Use for: paired-end, single-end and UMI-based scRNA-seq data

Pre-requisites:

- Data
    - Genomes (iGenomes for easy access)
    - SRA accession (.txt file with SRR ids)
    - url with raw data

- Defining parameters -> file.config (then converted to config.yml for R scripts)

- Tools (keep or install in $HOME)
    - sra-toolkit
    - FastQC
    - cutadapt
    - UMI tools
    - STAR
    - featureCounts
    - HTseq
    - MultiQC
  
- R packages
    - dplyr
    - yaml
    - ggplot2
    - Seurat
    - SeuratWrappers
    - reticulate::py_install(packages = 'umap-learn')
    - SingleCellExperiment
    - scater
    - batchelor
    - zinbwave
    - DESeq2